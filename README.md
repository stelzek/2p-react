### About react-patient-messenger-client

# System Requirements
> node --version 
> v4.2.2
> npm --version
> 3.4.0


## To install on your computer, type in command-line: ##
  
```
git clone https://bitbucket.org/onekit/react-patient-messenger-client.git
cd react-patient-messenger-client
npm install npm@3.4.0 -g
npm install
# run developer server
npm run server 
# run production build
npm run build  
```