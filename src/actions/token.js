"use strict";

import { UPDATE_TOKEN } from "../constants";

export function updateToken(token) {
  return { type: UPDATE_TOKEN, token };
}
