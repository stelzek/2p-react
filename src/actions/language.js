"use strict";

import { UPDATE_LANG } from "../constants";

export function updateLanguage(language) {
  return { type: UPDATE_LANG, language };
}
