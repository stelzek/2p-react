"use strict";
const DEBUG = true && process.env.NODE_ENV === "develop";

// Mode: <%= process.env.NODE_ENV %>

import localization from "./localization";
import React from "react";
import { render } from "react-dom";

// react-router
import Router from "react-router";
import { default as routes, history } from "./routes";

// redux
import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import reduxError from "redux-error";

// redux-api
import rest from "./rest";
import pubsub from "./utils/pubsub";
import "isomorphic-fetch";


let DevTools;
/* <% if(process.env.NODE_ENV === 'develop') { %> */

// Redux DevTools store enhancers
import { createDevTools, persistState } from "redux-devtools";
import LogMonitor from "redux-devtools-log-monitor";
import DockMonitor from "redux-devtools-dock-monitor";

DevTools = createDevTools(
  <DockMonitor
    defaultIsVisible={ false }
    toggleVisibilityKey="ctrl-h"
    changePositionKey="ctrl-q"
  >
    <LogMonitor theme="tomorrow" />
  </DockMonitor>
);
/* <% } %> */

rest.use("fetch", (url, opts)=> {
  return fetch(url, opts).then((r)=> r.json().then(
    (d)=> new Promise(
      (resolve, reject)=> {
        if (r.status >= 200 && r.status < 300) {
          resolve(d);
        } else {
          reject(d);
          pubsub.push("notification", d);
        }
      }
    )));
});

import reducers from "./reducers";

// Prepare store
const reducer = combineReducers({ ...rest.reducers, ...reducers });

let midleware = [applyMiddleware(reduxError, thunk)];

/* <% if(process.env.NODE_ENV === 'develop') { %> */
if (DEBUG) {
  const getDebugSessionKey = function () {
    const matches = window.location.href.match(/[?&]debug_session=([^&]+)\b/);
    return (matches && matches.length > 0) ? matches[1] : null;
  };
  midleware = midleware.concat(
    DevTools.instrument(),
    persistState(getDebugSessionKey())
  );
}
/* <% } %> */

const finalCreateStore = compose.apply(null, midleware)(createStore);

const initialState = window.$REDUX_STATE;
const store = initialState ? finalCreateStore(reducer, initialState) : finalCreateStore(reducer);
delete window.$REDUX_STATE;

rest.use("options", ()=> {
  // TODO: need to improve redux-api
  const token = store.getState().token.token;
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };
  if (token) {
    headers.Authorization = "Bearer " + token;
  }
  return { headers };
});

const childRoutes = routes(store);

const el = document.getElementById(
  window.APP_MOUNT_POINT || "react-main-mount"
) || document.body;

localization.setup({
  language: store.getState().language
});

window.store = store;

render(
  <Provider store={store}>
    <div>
      <Router key="ta-app" history={history} children={childRoutes}/>
      { DevTools ? <DevTools /> : null }
    </div>
  </Provider>,
  el
);
