import moment from "moment";
window.moment = moment;
const languages = {
  de: require("../lang/de.json")
};

let settings = {
  language: "de"
};

function language() {
  return (settings && settings.language) || "de";
}

function getText(text) {
  const lang = language();
  const dict = languages[lang];
  return (dict && dict[text]) ? dict[text] : text;
}

function setup(params) {
  settings = { ...settings, ...params };
  const lang = language();
  if (lang === "de") {
    require.ensure([
      "fullcalendar/dist/lang/de",
      "moment/locale/de"
    ], (require)=> {
      require("fullcalendar/dist/lang/de");
      require("moment/locale/de");
    });
    moment.locale("de");
  } else {
    moment.locale("en");
  }
}

getText.language = language;
getText.setup = setup;

export default getText;
