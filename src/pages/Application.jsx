"use strict";

import React, { PropTypes } from "react";
import NotificationSystem from "react-notification-system";
import "reset.css";
import "normalize.css";
import "font-awesome/css/font-awesome.css";
import "./Application.css";
import LoginMenu from "../components/LoginMenu";
import AuthMenu from "../components/AuthMenu";
import { connect } from "react-redux";
import rest from "../rest";
import { history } from "../routes";
import pubsub from "../utils/pubsub";
import { updateLanguage } from "../actions/language";

class Application extends React.Component {
  static propTypes = {
    time: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
    user: PropTypes.shape({
      full_name: PropTypes.string,
      image: PropTypes.string,
      position: PropTypes.string
    }),
    dispatch: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired,
    children: PropTypes.element
  };
  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(rest.actions.time());
    this.pubsubId && pubsub.unsubscribe(this.pubsubId);
    this.pubsubId = pubsub.subscribe("notification", this.onNotification.bind(this));
    this._notificationSystem = this.refs.notificationSystem;
  }
  componentWillUnmount() {
    this.pubsubId && pubsub.unsubscribe(this.pubsubId);
    this.pubsubId = null;
  }
  onLogout() {
    const { dispatch } = this.props;
    dispatch(rest.actions.logout((err)=> {
      !err && history.replace("/login");
    }));
  }
  onNotification(notification) {
    this._notificationSystem.addNotification({
      message: notification.message,
      level: "error",
      position: "bl",
      autoDismiss: "7"
    });
  }

  render() {
    const { user, token, time, language, dispatch } = this.props;
    let AppMenu;
    if (token) {
      AppMenu = (<AuthMenu
        username={ user.full_name || ""}
        image={ user.image || "" }
        position={ user.position || ""}
        time={ time }
        onLogout={ this.onLogout }
        language={ language }
      />);
    } else {
      AppMenu = (<LoginMenu title="2p-med.de" />);
    }

    const changeLang = (lang)=> ()=> dispatch(updateLanguage(lang));

    return (
      <div className="ApplicationPage">
        { AppMenu }
        <div className="ApplicationPage__content">
          { this.props.children }
        </div>
        <NotificationSystem ref="notificationSystem" />
        <ul className="ApplicationPage__langs">
          <li className={ language === "en" ? "hidden" : "" } >
            <a onClick={ changeLang("en") }>En</a>
          </li>
          <li className={ language === "de" ? "hidden" : "" } >
            <a onClick={ changeLang("de") }>De</a>
          </li>
        </ul>
    </div>);
  }
}

export default connect((state)=> ({
  user: state.user.data,
  token: state.token.token,
  time: state.time.data,
  language: state.language
}))(Application);
