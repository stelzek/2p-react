"use strict";

import React, { PropTypes } from "react";
import "./Login.css";
import { connect } from "react-redux";
import rest from "../rest";
import { history } from "../routes";
import Toggle from "../components/Toggle";

class Login extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      username: "doctor",
      password: "admin",
      remember: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  onSubmit(e) {
    const { username, password } = this.state;
    const { dispatch } = this.props;
    dispatch(rest.actions.login.post(username, password, (err)=> {
      if (!err) {
        dispatch(rest.actions.user());
        history.replace("/");
      }
    }));
    e.preventDefault();
    return false;
  }
  onChange(e) {
    this.setState({ remember: e.target.checked });
  }
  static onChangeValue(ctx, name) {
    return (event)=> {
      const { value } = event.target;
      ctx.setState({ [name]: value });
    };
  }
  render() {
    const { username, password, remember } = this.state;

    return (<div className="LoginPage">
      <form className="LoginPage__form" onSubmit={ this.onSubmit }>
        <label htmlFor="username">{ __("Username") }:</label>
        <input id="username"
          type="text"
          value={ username }
          onChange={Login.onChangeValue(this, "username")}
        />
        <label htmlFor="password">{ __("Password") }:</label>
        <input id="password"
          type="password"
          value={ password }
          onChange={Login.onChangeValue(this, "password")}
        />
        <div className="LoginPage__checkbox">
          <Toggle
            label={ __("Remember me") }
            checked={ remember }
            onChange={ this.onChange }
          />
        </div>
        <div className="LoginPage__submit">
          <input type="submit" value={ __("Login") }/>
        </div>
      </form>
    </div>);
  }
}

export default connect((state)=> ({
  language: state.language
}))(Login);
