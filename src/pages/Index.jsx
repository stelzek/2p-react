"use strict";

import React, { PropTypes } from "react";
import "./Index.css";
import PatientSearch from "../components/PatientSearch";
import PatientForm from "../components/PatientForm";
import PatientList from "../components/PatientList";
import ReactModal from "../components/ReactModal";
import { connect } from "react-redux";
import rest from "../rest";
import Calendar from "../components/Calendar";
import find from "lodash/collection/find";
import moment from "moment";
import map from "lodash/collection/map";
import { async } from "redux-api";
import ConfirmState from "../utils/confirm-state";

class Index extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    patients: PropTypes.shape({
      data: PropTypes.array.isRequired,
      error: PropTypes.any
    }).isRequired,
    appointments: PropTypes.array.isRequired,
    time: PropTypes.object.isRequired,
    language: PropTypes.string.isRequired,
    doctor: PropTypes.number
  };
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      query: "",
      patientId: null,
      selectPatientId: null,
      showForm: false,
      confirmDelete: -1
    };
    this.confirmDelete = ConfirmState(
      (val = -1)=> {
        this.setState({ confirmDelete: val });
      },
      (cb)=> {
        /* eslint no-console: 0 */
        console.log("delete");
        cb();
      }
    );
    this.addEvent = this.addEvent.bind(this);
    this.addPatient = this.addPatient.bind(this);
    this.loadPatient = this.loadPatient.bind(this);
    this.searchPatients = this.searchPatients.bind(this);
    this.updateEvent = this.updateEvent.bind(this);
    this.updatePatient = this.updatePatient.bind(this);
    this.onEditPatient = this.onEditPatient.bind(this);
    this.resetPatient = this.resetPatient.bind(this);
    this.onChangeFormShown = this.onChangeFormShown.bind(this);
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(rest.actions.user.sync(()=> {
      const { doctor } = this.props;
      doctor && dispatch(rest.actions.doctorAppointments.get(doctor));
    }));
  }
  onEditPatient(patientId) {
    this.setState({ patientId, selectPatientId: patientId });
  }
  onChangeFormShown(showForm=false) {
    const newState = { showForm };
    if (!showForm) {
      newState.patientId = null;
    }
    this.setState(newState);
  }
  getAppointments() {
    const { appointments } = this.props;
    const events = map(appointments, (event)=> {
      const { id, patient, start_time, end_time } = event;
      return {
        id, start: moment(start_time), end: moment(end_time),
        title: `${patient.title} ${patient.first_name} ${patient.last_name}`
      };
    });
    return events;
  }
  searchPatients(q, cb) {
    this.setState({ query: q });
    const { dispatch } = this.props;
    dispatch(rest.actions.user.sync(()=> {
      const { doctor } = this.props;
      if (doctor) {
        dispatch(rest.actions.doctorPatients.get(doctor, { keyword: q }, cb));
      } else {
        cb && cb("You are not doctor");
      }
    }));
  }
  addPatient(form, cb) {
    const { dispatch, language } = this.props;
    form.language || (form.language = language);
    return async(dispatch,
      rest.actions.user.sync,
      (cb)=> {
        const { doctor } = this.props;
        return rest.actions.doctorPatients.post(doctor, form, cb);
      }
    ).then((data)=> {
      cb(null, data);
      const item = find(data, { isNew: true });
      this.setState({ patientId: null, selectPatientId: item.id });
      this.onChangeFormShown();
    }, cb);
  }
  resetPatient() {
    this.setState({ patientId: null, selectPatientId: null });
  }
  loadPatient(id) {
    const { dispatch } = this.props;
    return async(
      dispatch,
      (cb)=> rest.actions.patients({ id }, cb)
    );
  }
  updatePatient(id, data) {
    const { dispatch } = this.props;
    async(dispatch,
      (cb)=> rest.actions.patients.put(id, data, cb)
    ).then(
      ()=> this.searchPatients(this.state.query)
    );
  }
  addEvent(patientId, date) {
    const { events } = this.state;
    const { patients } = this.props;
    const patient = find(patients, { id: patientId });
    if (!patient) { return false; }
    this.setState({
      events: events.concat({
        id: events.length,
        title: `${patient.first_name} ${patient.last_name}`,
        start: date
      })
    });
    return true;
  }
  updateEvent(id, date, minutes) {
    const { events } = this.state;
    const event = find(events, { id });
    if (!event) { return false; }
    const pos = events.indexOf(event);
    const date2 = moment(date).add(minutes, "minutes");
    const start = minutes > 0 ? date : date2;
    const end = minutes > 0 ? date2 : date;
    const newEvent = { id, title: event.title, start, end };
    const newEvents = events.slice(0, pos)
      .concat(newEvent)
      .concat(events.slice(pos + 1, events.length));
    this.setState({ events: newEvents });
  }
  render() {
    const { patients, language } = this.props;
    const { patientId, selectPatientId, showForm, confirmDelete } = this.state;
    const events = this.getAppointments();
    const onConfirmDelete = (id)=> this.confirmDelete.Open(id);
    const content = {
      position: "relative",
      top: "",
      left: "",
      right: "",
      bottom: "",
      width: "600px",
      height: "300px",
      margin: "100px auto 0"
    };
    const onDeletePatientAccept = ()=> this.confirmDelete.Confirm();
    const onDeletePatientCancel = ()=> this.confirmDelete.Hide();

    const ListComponent = (
      <PatientList
        selectPatientId={ selectPatientId }
        patients={ patients.data }
        onEditPatient={ this.onEditPatient }
      />
    );
    return (<div className="IndexPage">
      <div className="IndexPage__widget IndexPage__widget-left">
        <h1 className="IndexPage__widget__title">{ __("Patient list") }</h1>
        <PatientSearch
          showForm={ showForm }
          onChangeShown={ this.onChangeFormShown }
          language={ language }
          patients={ patients }
          patientId={ patientId }
          searchPatients={ this.searchPatients }
          listchildren={ ListComponent }
        >
          <PatientForm
            addPatient={ this.addPatient }
            updatePatient={ this.updatePatient }
            deletePatient={ onConfirmDelete }
            errors={ patients.error }
            language={ language }
            loadPatient={ this.loadPatient }
            patientId={ patientId }
          />
        </PatientSearch>
      </div>
      <div className="IndexPage__widget IndexPage__widget-center">
        <h1 className="IndexPage__widget__title">{ __("Personal organizer") }</h1>
        <Calendar
          language={ language }
          events={ events }
          updateEvent={ this.updateEvent }
          addEvent={ this.addEvent }
        />
      </div>
      <div className="IndexPage__widget IndexPage__widget-right">
        <h1 className="IndexPage__widget__title">{ __("Event details") }</h1>
      </div>
      <ReactModal
        isOpen={ confirmDelete !== -1 }
        onRequestClose={ onDeletePatientCancel }
        style={ { content } }
      >
        <h1>{ __("Delete patient") }</h1>
        <button onClick={ onDeletePatientAccept } >{ __("Delete") }</button>
        <button onClick={ onDeletePatientCancel } >{ __("Cancel") }</button>
      </ReactModal>
    </div>);
  }
}

export default connect((state)=> ({
  patients: state.doctorPatients,
  appointments: state.doctorAppointments.data,
  doctor: state.user.data.doctor,
  time: state.time.data,
  language: __.language()
}))(Index);
