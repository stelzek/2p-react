import StateMachine from "javascript-state-machine";
import isFunction from "lodash/lang/isFunction";
export default function(change, confirm) {
  if (!isFunction(change)) {
    throw new Error("1 argument 'change' isn't function");
  }
  if (!isFunction(confirm)) {
    throw new Error("2 argument 'confirm' isn't function");
  }
  const fnt = StateMachine.create({
    initial: "hide",
    events: [
      { name: "Open", from: "hide", to: "open" },
      { name: "Confirm", from: "open", to: "hide" },
      { name: "Hide", from: "open", to: "hide" }
    ],
    callbacks: {
      onenteropen(event, from, to, id) {
        change(id);
      },
      onleaveopen(event) {
        if (event !== "Confirm") { return; }
        let isAsync = true;
        let asyncProc = false;
        confirm((err, data)=> {
          isAsync = !(data === (void 0));
          if (asyncProc) {
            err ? fnt.transition.cancel() : fnt.transition();
          }
        });
        asyncProc = true;
        return isAsync ? StateMachine.ASYNC : (void 0);
      },
      onenterhide(event) {
        (event !== "startup") && change((void 0));
      }
    }
  });
  return fnt;
}
