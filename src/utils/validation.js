import reduce from "lodash/collection/reduce";
import each from "lodash/collection/each";
import filter from "lodash/collection/filter";
import isString from "lodash/lang/isString";
import isArray from "lodash/lang/isArray";

/* eslint max-len: 0 */
export const RX_EMAIL = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

function regexpValidation(val, rx, msg) {
  if (!rx.test(val)) {
    return msg;
  }
}

export function presense(name, msg) {
  return (val)=> {
    if (!val) {
      return msg || `Field ${name} shouldn't be empty`;
    }
  };
}

export function values(vals, name, msg) {
  return (val)=> {
    if (vals.indexOf(val) === -1) {
      return msg || `Field ${name} should be one of [${vals.join(" ,")}]`;
    }
  };
}

export function length(size, name, msg) {
  return (val)=> {
    if (!isString(val)) {
      return `Field ${name} isn't string`;
    }
    if (val.length !== size) {
      return msg || `Field ${name} should be equal ${size} size`;
    }
  };
}

export function minLength(size, name, msg) {
  return (val)=> {
    if (!isString(val)) {
      return `Field ${name} isn't string`;
    }
    if (val.length < size) {
      return msg || `Field ${name} should be more then ${size} size`;
    }
  };
}

export function validateRegExp(rx, name, msg) {
  return (val)=> {
    return regexpValidation(val, rx, msg || `Field ${name} isn't correct`);
  };
}

export function validateEmail(name, msg) {
  return (val)=> {
    return regexpValidation(
      val, RX_EMAIL, msg || `Field ${name} isn't correct email`);
  };
}

export default class Validation {
  constructor(...validators) {
    this.validators = validators;
  }
  validate(val) {
    const errors = reduce(this.validators, (memo, validate)=> {
      const err = validate(val);
      return err === (void 0) ? memo : memo.concat(err);
    }, []);
    return errors.length ? errors : (void 0);
  }
}

export default class {
  constructor() {
    this.validators = {};
    this.errors = {};
  }
  addValidator(name, ...validators) {
    this.validators[name] = new Validation(...validators);
  }
  addValidators(validators) {
    each(validators, (items, name)=> {
      const params = isArray(items) ? items : [items];
      this.addValidator(name, ...params);
    });
  }
  validate(name, val) {
    const ptr = this.validators[name];
    if (ptr) {
      const error = ptr.validate(val);
      if (!error && this.errors[name]) {
        this.errors[name] = (void 0);
      } else if (error) {
        this.errors[name] = error;
      }
      return error;
    }
  }
  isValid() {
    const errors = filter(this.errors, (error)=>  !!error);
    return !errors.length;
  }
}
