import each from "lodash/collection/each";
import isRegExp from "lodash/lang/isRegExp";

const subscribers = [];
export default {
  push(channel, message) {
    each(subscribers, (client)=> {
      if (!client) { return; }
      const { mask, cb } = client;
      const isSendMessage =
        (isRegExp(mask) && mask.test(channel)) ||
        (mask === channel);
      isSendMessage && cb && cb(message);
    });
  },
  subscribe(mask, cb) {
    subscribers[subscribers.length] = { mask, cb };
  },
  unsubscribe(id) {
    if (id < 0 || id >= subscribers.length) { return false; }
    const result = !!subscribers[id];
    result && (subscribers[id] = (void 0));
    return result;
  }
};
