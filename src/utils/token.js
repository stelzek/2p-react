"use strict";

import jscookie from "js-cookie";
const TOKEN_NAME = "$$$token";

const Token = {
  get() {
    return jscookie.get(TOKEN_NAME) || "";
  },
  set(value="") {
    jscookie.set(TOKEN_NAME, value);
    return value;
  }
};

export default Token;
