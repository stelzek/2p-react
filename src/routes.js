"use strict";

import rest from "./rest";
import createHistory from "history/lib/createHashHistory";
import Application from "./pages/Application";
import Index from "./pages/Index";
import Login from "./pages/Login";
import Token from "./utils/token";
export const history = createHistory();

function isAuth(state, replaceState, cb) {
  const token = Token.get();
  if (token) {
    cb && cb();
  } else {
    if (state.location.pathname !== "/login") {
      replaceState(state, "/login");
    }
    cb && cb();
    return "Token not found";
  }
}

export default function routes({ dispatch }) {
  return {
    path: "/",
    component: Application,
    onEnter(state, replaceState, cb) {
      const err = isAuth(state, replaceState);
      if (err) { return cb(); }
      dispatch(rest.actions.user((err)=> {
        // TODO
        if (err) { Token.set(); }
        isAuth(state, replaceState, cb);
      }));
    },
    indexRoute: {
      component: Index,
      onEnter(state, replaceState, cb) {
        isAuth(state, replaceState, cb);
      }
    },
    childRoutes: [{
      path: "/login",
      component: Login,
      onEnter(state, replaceState, cb) {
        const err = isAuth(state, replaceState);
        if (!err) {
          replaceState(state, "/");
        }
        cb();
      }
    }]
  };
}
