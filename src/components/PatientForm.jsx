"use strict";

import React, { PropTypes } from "react";

import reduce from "lodash/collection/reduce";
import find from "lodash/collection/find";
import pick from "lodash/object/pick";
import keys from "lodash/object/keys";
import camelCase from "lodash/string/camelCase";
import snakeCase from "lodash/string/snakeCase";

import Toggle from "./Toggle";
import Select2 from "./Select2";
import Validator from "./Validator";

import Form, {
  presense, minLength, length, validateEmail, validateRegExp
} from "../utils/validation";


import "./PatientForm.css";

export default class PatientForm extends React.Component {
  static propTypes = {
    addPatient: PropTypes.func.isRequired,
    updatePatient: PropTypes.func.isRequired,
    deletePatient: PropTypes.func.isRequired,
    loadPatient: PropTypes.func.isRequired,
    errors: PropTypes.any,
    language: PropTypes.string.isRequired,
    patientId: PropTypes.number
  };
  constructor(props = {}) {
    super(props);
    this.state = {
      ...PatientForm.FORM_STATE,
      showAllErrors: false,
      blockForm: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onDeletePatient = this.onDeletePatient.bind(this);
    this.form = new Form();
    const serverValidation = (_field)=> {
      const field = snakeCase(_field);
      const innerState = { error: this.props.errors };
      return (val)=> {
        if (innerState.error === this.props.errors && innerState.val !== val) {
          return (void 0);
        }
        innerState.error = this.props.errors;
        innerState.val = val;
        const errors = this.props.errors && this.props.errors.data;
        const item = find(errors, (item)=> item.property_path === field);
        return item && item.message;
      };
    };
    this.form.addValidators({
      gender: [
        presense("gender"),
        serverValidation("gender")
      ],
      firstName: [
        minLength(2, "firstName"),
        serverValidation("firstName")
      ],
      lastName: [
        minLength(2, "lastName"),
        serverValidation("lastName")
      ],
      phone: [
        length(12, "phone"),
        validateRegExp(/[\d]+/, "phone"),
        serverValidation("phone")
      ],
      email: [
        validateEmail("email"),
        serverValidation("email")
      ],
      insurance: [
        presense("insurance"),
        serverValidation("insurance")
      ],
      insuranceType: [
        presense("insuranceType"),
        serverValidation("insuranceType")
      ]
    });
  }
  componentDidMount() {
    const { patientId } = this.props;
    this.updateUserState(patientId);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.patientId !== this.props.patientId) {
      this.updateUserState(nextProps.patientId);
    }
  }
  onSubmit(e) {
    e.preventDefault();
    const { addPatient, patientId, updatePatient } = this.props;
    const formKeys = keys(PatientForm.FORM_STATE);
    const form = pick(this.state, formKeys);
    const data = reduce(form,
      (memo, val, key)=> ({ ...memo, [snakeCase(key)]: val }), {});
    data.title = "";
    if (patientId) {
      updatePatient(patientId, data);
    } else {
      addPatient(data);
    }
    this.setState({ showAllErrors: true });
  }
  onChangeField(field, prop) {
    this.__onChangeField || (this.__onChangeField = {});
    return this.__onChangeField[field] || (
      this.__onChangeField[field] =
        PatientForm.onChangeField(field, prop).bind(this));
  }
  onChangeSelect(field) {
    this.__onChangeSelect || (this.__onChangeSelect = {});
    return this.__onChangeSelect[field] || (
      this.__onChangeSelect[field] = PatientForm.onChangeSelect(field)
        .bind(this));
  }
  onDeletePatient() {
    const { deletePatient, patientId } = this.props;
    deletePatient(patientId);
  }
  static getGender() {
    return [
      { id: "", text: __("Gender") },
      { id: "1", text: __("Male") },
      { id: "0", text: __("Female") }
    ];
  }
  static getInsuranceType() {
    return [
      { id: "", text: __("Insurance type") },
      { id: "privat", text: __("Private") },
      { id: "gesetzlich", text: __("State") }
    ];
  }
  updateUserState(patientId) {
    if (!patientId) {
      this.setState(PatientForm.FORM_STATE);
    } else {
      const { loadPatient } = this.props;
      this.setState({ blockForm: true });
      loadPatient(patientId).then(
        (data)=> {
          const newState = reduce(data,
            (memo, value, key)=> ({ ...memo, [camelCase(key)]: value }), {});
          const keylist = keys(PatientForm.FORM_STATE);
          const newStateClean = pick(newState, keylist);
          newStateClean.blockForm = false;
          if (newStateClean.gender !== (void 0)) {
            newStateClean.gender = "" + newStateClean.gender;
          }
          this.setState(newStateClean);
        },
        ()=> this.setState({ blockForm: false }));
    }
  }
  static onChangeField(field, prop="value") {
    return function(e) {
      this.setState({ [field]: e.target[prop] });
    };
  }
  static onChangeSelect(field) {
    return function(item) {
      const value = item ? item.value : "";
      this.setState({ [field]: value });
    };
  }
  static INSURANCE = reduce(require("../json/insurance.json"),
    (memo, value)=> memo.concat({ text: value, id: value }), []);
  static FORM_STATE = {
    gender: "",
    firstName: "",
    lastName: "",
    phone: "",
    email: "",
    insurance: "",
    insuranceType: "",
    smsAlert: false,
    emailAlert: false
  };

  render() {
    const {
      firstName, lastName, phone, email,
      gender, insurance, insuranceType, smsAlert, emailAlert,
      showAllErrors, language, blockForm
    } = this.state;
    const { patientId } = this.props;
    const form = this.form;
    const title = !patientId ?
      __("Add new patient") :
      __("Edit patient");

    return (<form
      className={ "PatientForm" + (showAllErrors ? " PatientForm-error" : "") }
      onSubmit={ this.onSubmit }
    >
      <h2 className="PatientForm__title">{ title }:</h2>
      <div className="PatientForm__row PatientForm__row-3">
        <Validator error={ form.validate("gender", gender) }>
          <Select2
            className="PatientForm__select2"
            multiple={ false }
            name="gender"
            value={ gender }
            data={ PatientForm.getGender()  }
            placeholder={ __("Gender")}
            onChange={ this.onChangeField("gender") }
            language={ language }
            disabled={ blockForm }
          />

        </Validator>
        <Validator error={ form.validate("firstName", firstName) }>
          <input
            className="PatientForm__input"
            placeholder={ __("First name") }
            value={ firstName }
            onChange={ this.onChangeField("firstName") }
            disabled={ blockForm }
          />
        </Validator>
        <Validator error={ form.validate("lastName", lastName) }>
          <input
            className="PatientForm__input"
            placeholder={ __("Last name") }
            value={ lastName }
            onChange={ this.onChangeField("lastName") }
            disabled={ blockForm }
          />
        </Validator>
      </div>
      <div className="PatientForm__row PatientForm__row-2">
        <Validator error={ form.validate("phone", phone) }>
          <input
            className="PatientForm__input"
            placeholder={ __("Phone") }
            value={ phone }
            maxLength="12"
            onChange={ this.onChangeField("phone") }
            disabled={ blockForm }
          />
        </Validator>
        <Validator error={ form.validate("email", email) }>
          <input
            className="PatientForm__input"
            placeholder={ __("E-mail") }
            value={ email }
            onChange={ this.onChangeField("email") }
            disabled={ blockForm }
          />
        </Validator>
      </div>
      <div className="PatientForm__row">
        <Validator error={ form.validate("insurance", insurance) }>
          <Select2
            className="PatientForm__select2"
            multiple={ false }
            data={ PatientForm.INSURANCE }
            placeholder={ __("Insurance company")}
            name="insurance"
            value={ insurance }
            onChange={ this.onChangeField("insurance") }
            language={ language }
            disabled={ blockForm }
          />
        </Validator>
      </div>
      <div className="PatientForm__row">
        <Validator error={ form.validate("insuranceType", insuranceType) }>
          <Select2
            className="PatientForm__select2"
            multiple={ false }
            data={ PatientForm.getInsuranceType() }
            placeholder={ __("Insurance type")}
            name="insuranceType"
            value={ insuranceType }
            onChange={ this.onChangeField("insuranceType") }
            language={ language }
            disabled={ blockForm }
          />
        </Validator>
      </div>
      <div className="PatientForm__row toggles">
        <Toggle className="Toggle-cyan"
          label={ __("SMS notification") }
          checked={ smsAlert }
          onChange={ this.onChangeField("smsAlert", "checked") }
          disabled={ blockForm }
        />
        <Toggle
          className="Toggle-cyan"
          label={ __("E-mail notification") }
          checked={ emailAlert }
          onChange={ this.onChangeField("emailAlert", "checked") }
          disabled={ blockForm }
        />
      </div>
      <div className="PatientForm__row PatientForm__row-buttons">
        <button
          className="PatientForm__button"
          disabled={ !patientId }
          onClick={ this.onDeletePatient }
        >{ __("Delete") }
        </button>
        <button
          type="submit"
          className="PatientForm__button"
          disabled={ blockForm || !form.isValid() }
        >{ __("Save") }
        </button>
      </div>
    </form>);
  }
}
