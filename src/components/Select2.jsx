"use strict";
import React, { PropTypes } from "react";
import Select2Wrapper from "react-select2-wrapper";
import "react-select2-wrapper/css/select2.css";
import "./Select2.css";

export default class Select2 extends React.Component {
  static propTypes = {
    ...Select2Wrapper.propTypes,
    className: PropTypes.string,
    language: PropTypes.string,
    value: PropTypes.any.isRequired,
  };
  componentDidMount() {
    const el = this.refs.select2.el.select2();
    if (el) {
      if (el.val() !== this.props.value) {
        el.val(this.props);
      }
    }
  }
  componentWillReceiveProps(nextProps) {
    const el = this.refs.select2.el.select2();
    if (el && nextProps.value !== this.props.value) {
      if (el.val() !== nextProps.value) {
        el.val(nextProps.value).trigger("change");
      }
    }
  }
  render() {
    const { className } = this.props;
    return (
      <div className={ `Select2 ${className || "" }` }>
        <Select2Wrapper ref="select2" { ...this.props } />
      </div>
    );
  }
}
