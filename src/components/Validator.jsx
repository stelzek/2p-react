"use strict";

import React, { PropTypes } from "react";
import "./Validator.css";

export default class Validator extends React.Component {
  static propTypes = {
    children: PropTypes.element,
    className: PropTypes.string,
    active: PropTypes.bool,
    error: PropTypes.any
  };
  constructor(props) {
    super(props);
    this.state = {
      active: props.active === (void 0) ? props.active : false
    };
    this.onFocusout = this.onFocusout.bind(this);
  }
  componentDidMount() {
    this.refs.root.addEventListener("focusout", this.onFocusout);
  }
  componentWillUnmount() {
    this.refs.root.removeEventListener("focusout", this.onFocusout);
  }
  onFocusout() {
    this.setState({ active: true });
  }
  render() {
    const { children, className, error } = this.props;
    const { active } = this.state;
    const classVar = "Validator" +
      (active ? " Validator-active" : "") +
      (className ? (" " + className) : "") +
      (error ? " Validator-error" : "");
    return (
      <div ref="root" className={ classVar } >
        { children }
      </div>
    );
  }
}
