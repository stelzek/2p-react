import React, { PropTypes } from "react";

import moment from "moment";

export default class Time extends React.Component {
  static propTypes = {
    time: PropTypes.shape({
      diff: PropTypes.number.isRequired,
      server: PropTypes.object
    }),
    language: PropTypes.string.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      now: this.nowTime(props.time.diff || 0)
    };
  }
  componentDidMount() {
    this.updateTime();
  }
  componentWillUnmount() {
    if (this._handle) {
      clearTimeout(this._handle);
      this._handle = null;
    }
  }
  updateTime() {
    const { time } = this.props;
    this._handle && clearTimeout(this._handle);
    this._handle = setTimeout(()=> {
      this.setState({ now: this.nowTime(time.diff) });
      this.updateTime();
    }, 1000);
  }
  nowTime(diff) {
    const now = moment();
    return now.add(-diff, "s");
  }
  render() {
    const { time } = this.props;
    const { now } = this.state;
    const timeOutput = (time && time.server) ?
      (now.format("HH:mm:ss") +
      " " + __("Hours") + ", " +
      now.format("dddd, DD.MM.YYYY")) : "";
    return <div className="Time">{ timeOutput }</div>;
  }
}
