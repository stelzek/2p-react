import React, { PropTypes } from "react";
import "./PatientSearch.css";

export default class PatientSearch extends React.Component {
  static propTypes = {
    children: PropTypes.element,
    listchildren: PropTypes.element,
    patientId: PropTypes.number,
    searchPatients: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired,
    showForm: PropTypes.bool.isRequired,
    onChangeShown: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
    this.state = { q: "" };
    this.onSearch = this.onSearch.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }
  componentDidMount() {
    const { q } = this.state;
    const { searchPatients } = this.props;
    searchPatients(q);
  }
  componentWillUnmount() {
    this._waiting && clearTimeout(this._waiting);
    this._waiting = null;
  }
  onSearch(e) {
    const { searchPatients } = this.props;
    const q = e.target.value;
    this.setState({ q });
    this._waiting && clearTimeout(this._waiting);
    this._waiting = setTimeout(()=> {
      const { q } = this.state;
      searchPatients(q);
      this._waiting = null;
    }, 600);
  }
  onFocus() {
    const { onChangeShown } = this.props;
    onChangeShown(false);
  }
  render() {
    const { children, listchildren, patientId, showForm, onChangeShown } = this.props;
    const { q } = this.state;
    const focus = true;
    const onToggleCheckbox = ()=> onChangeShown(!showForm);

    return (<div className="PatientSearch">
      <div className="PatientSearch__toolbar">
        <div className="PatientSearch__field">
          <input className="PatientSearch__input"
            value={ q }
            onChange={ this.onSearch }
            onFocus={ this.onFocus }
            autoFocus={ focus }
            placeholder={ __("Search...") }
          />
        </div>
        <label
          className={
            `PatientSearch__icon ${
              patientId ? "PatientSearch__icon-edit" : "PatientSearch__icon-add"
            }`
          }
          htmlFor="PatientSearch__addFormToggler"
        >
          { (patientId ? "" : " +") }
        </label>
      </div>
      <input
        type="checkbox"
        className="PatientSearch__addFormToggler"
        id="PatientSearch__addFormToggler"
        checked={ showForm }
        onClick={ onToggleCheckbox }
      />
      <div className="PatientSearch__form">
        { children }
      </div>
      <div className="PatientSearch__list">
        { listchildren }
      </div>
    </div>);
  }
}
