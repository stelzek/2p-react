"use strict";

import React, { PropTypes } from "react";
import map from "lodash/collection/map";
import find from "lodash/collection/find";
import "./Select.css";

export default class Select extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    value: PropTypes.string.isRequired,
    options: PropTypes.array,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ""
    };
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    const { options } = this.props;
    const value = e.target.value;
    this.setState({ value });
    if (this.props.onChange) {
      this.props.onChange(find(options, { value }));
    }
  }
  render() {
    const { name, className, options } = this.props;
    const { value } = this.state;
    const item = find(options, { value });
    const Options = map(options,
      (item, i)=> <option key={i} value={ item.value }>{ item.label }</option>);
    const label = (item && item.label) || "";
    return (<div className={ `Select ${className}` }>
      <div className="Select__value">{ label }</div>
      <select
        className="Select__input"
        defaultValue={value}
        name={ name }
        onChange={ this.onChange }
      >{ Options }</select>
    </div>);
  }
}
