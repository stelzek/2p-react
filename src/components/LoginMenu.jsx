"use strict";

import React, { PropTypes } from "react";
import "./LoginMenu.css";

export default class LoginMenu extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.object
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { title } = this.props;
    return (<div className="LoginMenu">
      <input
        className="LoginMenu__toggle"
        type="checkbox"
        name="LoginMenu__toggle"
        id="LoginMenu__toggle"
      />
      <div className="LoginMenu__container">
        <div className="LoginMenu__menu">
          <a className="LoginMenu__login" href="/dist/index.html">Login</a>
        </div>
        <div className="LoginMenu__logo-wrapper">
          <a className="LoginMenu__icon-logo" title={ title } href="/">
          </a>
        </div>
      </div>
      <label className="LoginMenu__label" htmlFor="LoginMenu__toggle"></label>
      { this.props.children }
    </div>);
  }
}
