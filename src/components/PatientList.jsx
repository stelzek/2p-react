"use strict";

import React, { PropTypes } from "react";
import keys from "lodash/object/keys";
import map from "lodash/collection/map";
import groupBy from "lodash/collection/groupBy";
import flatten from "lodash/array/flatten";
import "./PatientList.css";
import $ from "jquery";
import "jquery-ui/draggable";
import find from "lodash/collection/find";

export default class PatientList extends React.Component {
  static propTypes = {
    selectPatientId: PropTypes.any,
    patients: PropTypes.array.isRequired,
    onEditPatient: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
    this.patientsBlock = this.patientsBlock.bind(this);
    this.letterBlock = this.letterBlock.bind(this);
  }
  componentDidUpdate() {
    const { patients } = this.props;
    const $items = $(".PatientList__patients__item", this.refs.list);
    $items.each(function() {
      const $el = $(this);
      const id = $el.data("id");
      const item = find(patients, { id });
      if (!item) { return; }
      $(this).draggable({
        zIndex: 999,
        revert: true,
        revertDuration: 0,
        appendTo: ".ApplicationPage"
      });
    });
  }
  patientsBlock(p, i) {
    const { onEditPatient, selectPatientId } = this.props;
    const editPatient = ()=> onEditPatient(p.id);
    const className = "PatientList__patients__item" +
      ` PatientList__patients__item-${i % 2}` +
      ((selectPatientId === p.id) ? ` PatientList__patients__item-active` : "");
    return (
      <li
        key={`p${i}`}
        className={ className }
        data-id={ p.id }
        onClick={ editPatient }
      >
        <a>{ p.first_name } { p.last_name }</a>
      </li>
    );
  }
  letterBlock(patients, letter) {
    const Patients = map(patients, this.patientsBlock);
    return [(
      <li className="PatientList__list__char" key={ letter }>
        { letter }
      </li>
    ), (
      <li className="PatientList__list__patient" key={ `list-${letter}` }>
        <ul className="PatientList__patients">
          { Patients }
        </ul>
      </li>
    )];
  }
  render() {
    $(".PatientList__patients__item", this.refs.list).each(function() {
      const $el = $(this);
      if ($el.data("ui-draggable")) {
        $el.draggable("destroy");
      }
    });
    const { patients } = this.props;
    const group = groupBy(patients,
      (item)=> item && item.last_name && item.last_name[0]);

    const keylist = keys(group);
    keylist.sort();
    const Data = flatten(map(keylist,
      (key)=> this.letterBlock(group[key], key)));

    return (
      <div className="PatientList">
        <ul className="PatientList__list" ref="list">
          { Data }
        </ul>
      </div>
    );
  }
}
