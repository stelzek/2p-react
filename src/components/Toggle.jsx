import React, { PropTypes } from "react";
import "./Toggle.css";

export default class Toggle extends React.Component {
  static propTypes = {
    checked: PropTypes.bool.isRequired,
    className: PropTypes.string,
    onChange: PropTypes.func,
    label: PropTypes.string
  };
  render() {
    const { label, checked, onChange, className } = this.props;
    return (
      <label className={`Toggle ${className || ""}`}>
        <input
          type="checkbox"
          checked={ checked }
          onChange={ onChange }
          className="Toggle__item"
        />
        <span className="Toggle__btn" />
        <span className="Toggle__text">{ label }</span>
      </label>
    );
  }
}
