
import React, { PropTypes } from "react";
import { Link } from "react-router";
import "./AuthMenu.css";
import Time from "./Time";

export default class AuthMenu extends React.Component {
  static propTypes = {
    username: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
    time: PropTypes.object.isRequired,
    onLogout: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired
  };
  render() {
    const { username, image, position, time, language, onLogout } = this.props;
    const backgroundImage = image ? `url(${image})` : "";
    return (<header className="AuthMenu">
      <div className="AuthMenu__avatar">
        <Link
          className="AuthMenu__avatar__img"
          style={{ backgroundImage }}
          to="/"
        />
        <div className="AuthMenu__avatar__info">
          <p className="AuthMenu__avatar__name">{ username }</p>
          <p className="AuthMenu__avatar__role">{ position }</p>
        </div>
      </div>

      <div className="AuthMenu__logo" />

      <ul className="AuthMenu__menu">
        <li>
          <span className="AuthMenu__menu__item">
            <Time time={time} language={ language } />
          </span>
        </li>
        <li>
          <a className="AuthMenu__menu__item AuthMenu__menu__item-dropdown">{ __("Settings") }</a>
          <ul className="AuthMenu__dropdowncontent">
            <li>
              <a href="#">{ __("Practice Details") }</a>
              <ul>
                <li><a href="#">{ __("Opening times") }</a></li>
                <li><a href="#">{ __("Name and Address") }</a></li>
                <li><a href="#">{ __("Phone and Email") }</a></li>
                <li><a href="#">{ __("Add Logo") }</a></li>
              </ul>
            </li>
            <li>
              <a href="#">{ __("User") }</a>
              <ul>
                <li><a href="#">{ __("User Data") }</a></li>
                <li><a href="#">{ __("Login and Password") }</a></li>
                <li><a href="#">{ __("Add profile picture") }</a></li>
              </ul>
            </li>
            <li><a href="#">{ __("Import patient per xls") }</a></li>
            <li><a href="#">{ __("Notification Settings") }</a>
              <ul>
                <li><a href="#">{ __("Reminder SMS Time") }</a></li>
                <li><a href="#">{ __("SMS templates") }</a></li>
                <li><a href="#">{ __("Consumption and limit") }</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a className="AuthMenu__menu__item" onClick={ onLogout }>{ __("Logout")}</a>
        </li>
      </ul>
    </header>);
  }
}
