"use strict";

import React, { PropTypes } from "react";
import $ from "jquery";
import "fullcalendar";
import "fullcalendar/dist/fullcalendar.css";
import "./Calendar.css";
import moment from "moment";

export default class Calendar extends React.Component {
  static propTypes = {
    events: PropTypes.array.isRequired,
    updateEvent: PropTypes.func.isRequired,
    addEvent: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired
  };
  componentDidMount() {
    this.initCalendar();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.events !== this.props.events) {
      const c = this.getCalendar();
      c && c.refetchEvents();
    }
    if (prevProps.language !== this.props.language) {
      const calendar = this.getCalendarNode(false);
      if (calendar) {
        this.destroyCalendar();
        this.initCalendar();
      }
    }
  }
  componentWillUnmount() {
    this.destroyCalendar();
  }
  getCalendar() {
    return this.getCalendarNode().data("fullCalendar");
  }
  getCalendarNode(getOrCreate=true) {
    const $el = $(this.refs.calendar);
    const $childs = $el.children().eq(0);
    if ($childs.length) {
      return $childs;
    } else if (!getOrCreate) {
      return (void 0);
    } else {
      const $node = $("<div>");
      $el.append($node);
      return $node;
    }
  }
  initCalendar() {
    const { updateEvent, addEvent, language } = this.props;
    this.getCalendarNode().fullCalendar({
      header: {
        left: "prev,next today",
        center: "title",
        right: "agendaDay,agendaWeek"
      },
      lang: language,
      axisFormat: "HH:mm",
      timeFormat: {
        agenda: "HH:mm"
      },
      scrollTime: "08:00:00",
      defaultDate: moment().format("YYYY-MM-DD"),
      defaultView: "agendaWeek",
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      droppable: true,
      allDaySlot: false,
      slotDuration: "00:05:00",
      defaultTimedEventDuration: "00:15:00",
      eventDrop(event /* , delta, revert */) {
        const diff = event.end.diff(event.start, "minutes");
        updateEvent(event.id, event.start, diff);
      },
      drop(date, event, opts) {
        const { helper } = opts;
        const id = helper.data("id");
        addEvent(id, date);
      },
      events: this.loadEvents.bind(this)
    });
  }
  destroyCalendar() {
    const $node = this.getCalendarNode(false);
    if ($node) {
      $node.fullCalendar("destroy");
      $node.remove();
    }
  }
  loadEvents(start, end, flag, cb) {
    const { events } = this.props;
    cb(events);
  }
  render() {
    return (<div className="Calendar" ref="calendar"></div>);
  }
}
