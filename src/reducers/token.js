"use strict";

import { UPDATE_TOKEN } from "../constants";
import Token from "../utils/token";
import rest from "../rest";

export const initialState = {
  token: Token.get()
};

export default {
  [UPDATE_TOKEN](state, action) {
    Token.set(action.token);
    return { token: Token.get() };
  },
  [rest.events.login.actionSuccess](state, action) {
    Token.set(action.data.token);
    return { token: Token.get() };
  },
  [rest.events.logout.actionSuccess]() {
    Token.set();
    return { token: Token.get() };
  }
};
