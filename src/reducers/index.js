"use strict";

function makeReducer(reducer) {
  return (state=reducer.initialState, action)=> {
    const func = reducer.default && reducer.default[action.type];
    return func ? func(state, action) : state;
  };
}

export default {
  token: makeReducer(require("./token")),
  language: makeReducer(require("./language"))
};
