"use strict";

import { UPDATE_LANG } from "../constants";
import localization from "../localization";

export const initialState = "de";

export default {
  [UPDATE_LANG](state, action) {
    localization.setup({ language: action.language });
    return action.language;
  }
};
