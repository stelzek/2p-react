export default function(actions) {
  /* eslint no-use-before-define: 0 */
  return new Promise((resolve, reject)=> {
    this.dispatch(this.actions.user.sync((err)=> {
      if (err) { return reject(err); }
      const { doctor } = this.getState().user.data;
      !doctor ? reject("You are not doctor") : resolve(doctor);
    }));
  });
}
