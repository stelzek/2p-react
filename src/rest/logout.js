export default {
  url: "/api/logout",
  virtual: true,
  postfetch: [
    function({ dispatch, actions }) {
      dispatch(actions.user.reset());
      dispatch(actions.doctorPatients.reset());
    }
  ]
};
