import moment from "moment";

export default {
  url: "/api/time",
  transformer(d) {
    const now = moment();
    if (!d) {
      return { diff: 0 };
    } else {
      const time = moment(d.data);
      return { server: time, diff: now.diff(time, "s") };
    }
  }
};
