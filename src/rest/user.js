import pick from "lodash/object/pick";
import { updateLanguage } from "../actions/language";

export default {
  url: "/api/profile",
  prefetch: [
    function({ getState }, cb) {
      const token = getState().token.token;
      cb(token ? (void 0) : new Error("Token isn't define"));
    }
  ],
  postfetch: [
    function({ dispatch, getState }) {
      const { user: { data } } = getState();
      const { language } = data || {};
      dispatch(updateLanguage(language));
    }
  ],
  transformer(d) {
    if (d && d.data) {
      return pick(d.data,
        "doctor", "full_name", "id", "image", "language", "position"
      );
    }
    return {};
  }
};
