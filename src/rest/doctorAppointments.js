
import defaults from "lodash/object/defaults";

export default {
  url: "/api/doctors/(:id)/appointments",
  transformer(d) {
    if (d && d.data) {
      return d.data;
    }
    return [];
  },
  helpers: {
    get(id, params={}) {
      const { page, limit, orderBy, orderDir } = defaults(params, {
        page: 1, limit: 50, orderBy: "id", orderDir: "ASC"
      });
      return [{ id, page, limit, orderBy, orderDir }];
    }
  }
};
