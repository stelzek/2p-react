"use strict";
import reduxApi from "redux-api";

const rest = reduxApi({
  login: require("./login"),
  logout: require("./logout"),
  user: require("./user"),
  time: require("./time"),
  doctorPatients: require("./doctorPatients"),
  doctorAppointments: require("./doctorAppointments"),
  doctorAppointmentsCalendar: require("./doctorAppointmentsCalendar"),
  patients: require("./patients")
});

export default rest;
