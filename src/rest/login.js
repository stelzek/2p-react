export default {
  url: "/api/login_check",
  virtual: true,
  helpers: {
    post(_username, _password) {
      const body = JSON.stringify({ _username, _password });
      return [null, { body, method: "post" }];
    }
  }
};
