import pick from "lodash/object/pick";

export default {
  url: "/api/patients/(:id)",
  virtual: true,
  helpers: {
    put(id, params) {
      const jsonData = pick(params,
        "first_name", "last_name", "gender", "email",
        "phone", "account_id", "language", "notify_email", "notify_sms");
      const body = JSON.stringify(jsonData);
      return [{ id }, { body, method: "put" }];
    }
  },
  transformer(d) {
    return d ? d.data : {};
  }
};
