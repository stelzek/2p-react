import map from "lodash/collection/map";
import sortBy from "lodash/collection/sortBy";
import pick from "lodash/object/pick";
import defaults from "lodash/object/defaults";

function extractor(item) {
  return pick(item,
    "id",
    "title",
    "first_name",
    "last_name",
    "gender",
    "language"
  );
}

function sorter(item) {
  return item.first_name + " " + item.last_name;
}

export default {
  url: "/api/doctors/(:id)/patients",
  transformer(d, prev=[], action={}) {
    const r = action.request || {};
    if (r.params && r.params.method === "post") {
      const item = extractor(d.data);
      item.isNew = true;
      const prevArray = map(prev, extractor).concat(item);
      return sortBy(prevArray, sorter);
    }
    if (d && d.data) {
      const list = map(d.data, extractor);
      return sortBy(list, sorter);
    }
    return prev;
  },
  helpers: {
    get(id, params={}) {
      const { keyword, page, limit, orderBy, orderDir } = defaults(params, {
        keyword: "", page: 1, limit: 50, orderBy: "id", orderDir: "ASC"
      });
      return [{ id, keyword, page, limit, orderBy, orderDir }];
    },
    post(id, params={}) {
      const jsonData = pick(params,
        "title", "first_name", "last_name", "gender", "email",
        "phone", "account_id", "language", "notify_email", "notify_sms");
      const body = JSON.stringify(jsonData);
      return [{ id }, { body, method: "post" }];
    }
  }
};
