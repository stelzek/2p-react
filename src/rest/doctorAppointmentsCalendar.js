import moment from "moment";
import omit from "lodash/object/omit";
import getDoctor from "./helpers/getDoctor";

export default {
  url: "/api/doctors/(:id)/appointments/calendar",
  options: (url, opts, getState)=> {
    /* eslint camelcase: 0 */
    const { time } = getState();
    let { start_time, end_time } = opts.params || {};
    if (!start_time) {
      start_time = moment(time.data.server);
    }
    if (!end_time) {
      end_time = moment(start_time).add(1, "week");
    }
    const clearOpt = omit(opts, "params");
    const body = JSON.stringify({
      start_time: start_time.format("YYYY-MM-DD hh:mm:ss"),
      end_time: start_time.format("YYYY-MM-DD hh:mm:ss")
    });
    return { ...clearOpt, body };
  },
  prefetch: [
    function({ dispatch, actions, getState }, cb) {
      const { time } = getState();
      if (time.sync) {
        return cb();
      } else {
        dispatch(actions.time.sync(cb));
      }
    }
  ],
  helpers: {
    post(start_time, end_time) {
      return (cb)=> getDoctor.call(this).then((doctor)=> {
        return cb(null, [{ id: doctor }, {
          method: "post",
          params: { start_time, end_time }
        }]);
      });
    }
  }
};
